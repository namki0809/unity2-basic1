﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlerCSharp.Ch05_04
{
	class Program
	{
		//클래스 정의
		public class Product
		{
			public const int ConstPrice = 1000;     //const 필드 설정
			public readonly int ReadOnlyPrice;      //reonly 필드 설정

			public Product()
			{
				this.ReadOnlyPrice = 2000;          //생성자에서 초기값 설정
			}

			public Product(int price)
			{
				this.ReadOnlyPrice = price;         //외부에서 받은 값으로 설정
			}
		}

		static void Main(string[] args)
		{
			//상수값 가져오기 : 상수는 정적인 멤버 이므로 객체를 생성할 필요가 없다.
			Console.WriteLine("ConstPrice={0}", Product.ConstPrice);

			//기본 생성자로 설정된 값
			Product item1 = new Product();
			Console.WriteLine("new Product() : ReadOnlyPrice={0}", item1.ReadOnlyPrice);

			//생성자에서 특정값을 받음.
			Product item2 = new Product(3000);
			Console.WriteLine("new Product(3000) : ReadOnlyPrice={0}", item2.ReadOnlyPrice);
		}
	}
}