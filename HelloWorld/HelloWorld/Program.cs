﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlerCSharp.Ch03_01
{
	class Program
	{
		/// <summary>
		/// 클래스 / 메서드 등의 XML 주석
		/// </summary>
		/// <param name="args"></param>
		static void Main(string[] args)
		{
			//단일 라인 주석
			Console.WriteLine("Hello World !");
		}
	}
}